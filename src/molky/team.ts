import {IShuffler} from "./shuffler";

export class Team {
    constructor(private readonly shuffler: IShuffler) {
    }

    groupPlayersInTeams(players: string[]): string[][] {
        const teamA: string[] = [];
        const teamB: string[] = [];
        const teamC: string[] = [];

        players = this.shuffler.shuffle(players);

        if (players.length % 3 !== 0) {
            players.forEach((player, index) => {
                switch (index % 2) {
                    case 0:
                        teamA.push(player);
                        break;
                    case 1:
                        teamB.push(player);
                        break;
                }
            });

            return [teamA, teamB]
        }

        players.forEach((player, index) => {
            switch (index % 3) {
                case 0:
                    teamA.push(player);
                    break;
                case 1:
                    teamB.push(player);
                    break;
                case 2:
                    teamC.push(player);
                    break;

            }
        });

        return [teamA, teamB, teamC];
    }
}