import {Rules} from "./rules.enum";

export type Save = {
    teams: string[][],
    activeTeam: number,
    activeTeamMember: number[],
    thrusts: number[][][],
    rules: Rules[]
};

export interface IStore {
    persist(save: Save): void;
    load(): Save;
    saveExists(): boolean;
}

export class Store implements IStore {
    load(): Save {
        return JSON.parse(localStorage.getItem(this.storageKey)!);
    }

    private storageKey = 'molky';

    persist(save: Save): void {
        localStorage.setItem(this.storageKey, JSON.stringify(save));
    }

    saveExists(): boolean {
        return localStorage.getItem(this.storageKey) !== null;
    }

}