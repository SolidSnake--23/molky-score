import {scoreOf} from "./score";
import {Save} from "./store";
import {Rules} from "./rules.enum";

export class Game {

    constructor(
        private readonly teams: string[][],
        private thrusts: number[][][],
        private activeMemberPerTeam: number[],
        private activeTeam: number,
        private rules: Rules[],) {
    }

    static newGame(teams: string[][], rules: Rules[] = []): Game {
        const thrusts = teams.map(() => []);
        const activeMemberPerTeam = teams.map(() => 0);
        return new Game(teams, thrusts, activeMemberPerTeam, 0, rules)
    }

    static fromSave(save: Save): Game {
        return new Game(save.teams, save.thrusts, save.activeTeamMember, save.activeTeam, [])
    }

    generateSave(): Save {
        return {
            teams: this.teams,
            activeTeam: this.activeTeam,
            activeTeamMember: this.activeMemberPerTeam,
            thrusts: this.thrusts,
            rules: this.rules
        }
    }

    getScores(): number[] {
        return this.thrusts.map(thrusts => scoreOf(thrusts));
    }

    getNextPlayer(): string {
        return this.teams[this.activeTeam][this.activeMemberPerTeam[this.activeTeam]];
    }

    setThrust(pinsValue: number[]) {
        this.thrusts[this.activeTeam].push(pinsValue);
        if (scoreOf(this.thrusts[this.activeTeam].slice(-3)) == 0 && scoreOf(this.thrusts[this.activeTeam]) > 25) {
            this.thrusts[this.activeTeam].pop();
            this.thrusts[this.activeTeam].push([-1 * (scoreOf(
                this.thrusts[this.activeTeam]))%25]);
        }
        this.activeMemberPerTeam[this.activeTeam] = (this.activeMemberPerTeam[this.activeTeam] + 1) % this.teams[this.activeTeam].length
        this.activeTeam = (this.activeTeam + 1) % this.teams.length;
    }

    undo(): void {
        this.activeTeam -= 1;
        this.activeTeam = this.activeTeam < 0 ? this.teams.length-1 : this.activeTeam;
        this.activeMemberPerTeam[this.activeTeam] = (this.activeMemberPerTeam[this.activeTeam] - 1) < 0 ? this.teams[this.activeTeam].length-1 : (this.activeMemberPerTeam[this.activeTeam] - 1);
        this.thrusts[this.activeTeam].pop();
    }

    getTeamNames() {
        return this.teams.map(teamMembers => teamMembers.map(([playerFirstLetter, ...restOftTheName]: string) => playerFirstLetter.toUpperCase() + restOftTheName.join('').toLowerCase()).join(', '));
    }

    totalNbOfPlayers() {
        return this.teams.reduce((nbOfPlayers, team) => nbOfPlayers + team.length, 0);
    }
}