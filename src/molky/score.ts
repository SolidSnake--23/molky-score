export function scoreOf(thrusts: number[][]) {
    return thrusts.reduce((sum, pinValues) => {
        const newScore = sum + (pinValues.length === 1 ? pinValues[0] : pinValues.length);
        return newScore > 50 ? 25 : newScore;
    }, 0);
}