export interface IShuffler {
    shuffle<T>(collection: T[]): T[];
}

export class Shuffler implements IShuffler {

    constructor(private readonly random: () => number) {
    }

    shuffle<T>(collection: T[]): T[] {
        return collection.sort(this.random);
    }
}