import {scoreOf} from "../molky/score";

describe('Score', function () {
    it('should sum nb of fallen pin', function () {
        expect(scoreOf([
            [1, 2],
            [12, 11],
            [1, 2, 3, 4, 5],
        ])).toBe(9);
    });
    it('should sum the value of fallen pin when only one fall', function () {
        expect(scoreOf([
            [1, 2],
            [12, 11],
            [1, 2, 3, 4, 5],
            [12]
        ])).toBe(21);
    });
    it('should return 25 when getting over 50', function () {
        expect(scoreOf([
            [10],
            [10],
            [10],
            [10],
            [12],
        ])).toBe(25);
    });
    it('should return 25 again when getting over 50 twice', function () {
        expect(scoreOf([
            [10],
            [10],
            [10],
            [10],
            [12],
            [10],
            [10],
            [10],
        ])).toBe(25);
    });
});