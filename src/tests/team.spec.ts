import {Team} from "../molky/team";
import {IShuffler} from "../molky/shuffler";

const identityShuffler: IShuffler = {shuffle<T>(collection: T[]): T[] {
        return collection;
    }};

describe('Team', function () {
    it('should group players in 3 teams when teams can be even', function () {
        const players = [
            'florian',
            'alice',
            'rami',
            'sherry',
            'marine',
            'julien'
        ];

        const team = new Team(identityShuffler);

        const teams = team.groupPlayersInTeams(players);
        expect(teams).toEqual([
            ['florian', 'sherry'],
            ['alice', 'marine'],
            ['rami', 'julien'],
            ]
        );
    });
    it('should group players in 2 teams can not be divide in 3 teams', function () {
        const players = [
            'florian',
            'alice',
            'rami',
            'sherry',
            'marine'
        ];

        const team = new Team(identityShuffler);

        const teams = team.groupPlayersInTeams(players);
        expect(teams).toEqual([
            ['florian', 'rami', 'marine'],
            ['alice', 'sherry'],
            ]
        );
    });
});