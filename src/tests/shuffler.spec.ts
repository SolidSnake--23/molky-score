import {Shuffler} from "../molky/shuffler";

describe('Shuffle', function () {
    it('should shuffle array', function () {
        const players = [
            'florian',
            'alice',
            'rami',
            'sherry',
            'marine',
            'julien',
        ];

        const shuffle = new Shuffler(function () {
            return arguments[1]
        });

        const shuffledPlayers = shuffle.shuffle(players);
        expect(shuffledPlayers).toEqual(
            [
                'florian',
                'alice',
                'rami',
                'sherry',
                'marine',
                'julien',
            ]
        );
    });
});