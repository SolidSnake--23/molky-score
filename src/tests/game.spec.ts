import {Game} from "../molky/game";
import {Save} from "../molky/store";
import {Rules} from "../molky/rules.enum";


describe('Game', function () {
    it('should Molky', function () {
        const teamA = [
            'florian',
            'alice'
        ];
        const teamB = [
            'rami',
            'sherry'
        ];
        const teamC = [
            'marine',
            'julien',
            'neo'
        ];

        const game = Game.newGame([teamA, teamB, teamC]);

        expect(game.getScores()).toEqual([0, 0, 0]);
        expect(game.getNextPlayer()).toEqual('florian');

        game.setThrust([1, 2]);

        expect(game.getScores()).toEqual([2, 0, 0]);
        expect(game.getNextPlayer()).toEqual('rami');

        game.setThrust([11]);

        expect(game.getScores()).toEqual([2, 11, 0]);
        expect(game.getNextPlayer()).toEqual('marine');

        game.setThrust([4, 5]);

        expect(game.getScores()).toEqual([2, 11, 2]);
        expect(game.getNextPlayer()).toEqual('alice');

        game.setThrust([4]);

        expect(game.getScores()).toEqual([6, 11, 2]);
        expect(game.getNextPlayer()).toEqual('sherry');

        game.setThrust([3, 4, 6]);

        expect(game.getScores()).toEqual([6, 14, 2]);
        expect(game.getNextPlayer()).toEqual('julien');

        game.setThrust([9]);

        expect(game.getScores()).toEqual([6, 14, 11]);
        expect(game.getNextPlayer()).toEqual('florian');

        game.setThrust([12]);

        expect(game.getScores()).toEqual([18, 14, 11]);
        expect(game.getNextPlayer()).toEqual('rami');

        game.setThrust([0]);

        expect(game.getScores()).toEqual([18, 14, 11]);
        expect(game.getNextPlayer()).toEqual('neo');

        game.setThrust([12]);

        expect(game.getScores()).toEqual([18, 14, 23]);
        expect(game.getNextPlayer()).toEqual('alice');
    });
    it('should create Molky save', function () {
        const teamA = [
            'florian',
            'alice'
        ];
        const teamB = [
            'rami',
            'sherry'
        ];
        const teamC = [
            'marine',
            'julien',
            'neo'
        ];

        const game = Game.newGame([teamA, teamB, teamC], [Rules.MISS_3_IN_ROW_DROP_SCORE_TO_25]);
        game.setThrust([1, 2]);
        game.setThrust([11]);
        game.setThrust([4, 5]);
        game.setThrust([4]);
        game.setThrust([3, 4, 6]);
        game.setThrust([9]);
        game.setThrust([12]);
        game.setThrust([0]);
        game.setThrust([12]);

        const expectedSave: Save = {
            teams: [teamA, teamB, teamC],
            activeTeam: 0,
            activeTeamMember: [1, 1, 0],
            rules: [Rules.MISS_3_IN_ROW_DROP_SCORE_TO_25],
            thrusts: [
                [
                    [1, 2],
                    [4],
                    [12]
                ], [
                    [11],
                    [3, 4, 6],
                    [0]
                ], [
                    [4, 5],
                    [9],
                    [12]
                ]
            ],
        };
        expect(game.generateSave()).toEqual(expectedSave);
    });
    it('should load Molky save', function () {
        const teamA = [
            'florian',
            'alice'
        ];
        const teamB = [
            'rami',
            'sherry'
        ];
        const teamC = [
            'marine',
            'julien',
            'neo'
        ];


        const save: Save = {
            teams: [teamA, teamB, teamC],
            activeTeam: 0,
            activeTeamMember: [1, 1, 0],
            rules: [],
            thrusts: [
                [
                    [1, 2],
                    [4],
                    [12]
                ], [
                    [11],
                    [3, 4, 6],
                    [0]
                ], [
                    [4, 5],
                    [9],
                    [12]
                ]
            ],
        };
        const game = Game.fromSave(save);

        expect(game.getScores()).toEqual([18, 14, 23]);
        expect(game.getNextPlayer()).toEqual('alice');
    });
    it('should undo Molky', function () {
        const teamA = [
            'florian',
            'alice'
        ];
        const teamB = [
            'rami',
            'sherry'
        ];
        const teamC = [
            'marine',
            'julien',
            'neo'
        ];


        const save: Save = {
            teams: [teamA, teamB, teamC],
            activeTeam: 0,
            activeTeamMember: [1, 1, 0],
            rules: [],
            thrusts: [
                [
                    [1, 2],
                    [4],
                    [12]
                ], [
                    [11],
                    [3, 4, 6],
                    [0]
                ], [
                    [4, 5],
                    [9],
                    [12]
                ]
            ],
        };
        const game = Game.fromSave(save);

        game.undo();

        expect(game.getScores()).toEqual([18, 14, 11]);
        expect(game.getNextPlayer()).toEqual('neo');

        game.undo();

        expect(game.getScores()).toEqual([18, 14, 11]);
        expect(game.getNextPlayer()).toEqual('rami');

        game.undo();


    });
    it('should retrieve team names', () => {
        const teamA = [
            'florian',
            'alice'
        ];
        const teamB = [
            'rami',
            'sherry'
        ];
        const teamC = [
            'marine',
            'julien',
            'neo'
        ];

        const game = Game.newGame([teamA, teamB, teamC]);

        expect(game.getTeamNames()).toEqual([
            'Florian, Alice',
            'Rami, Sherry',
            'Marine, Julien, Neo'
        ])
    })

    it('should return nb of players', function () {
        const game = Game.newGame([]);

        expect(game.totalNbOfPlayers()).toBe(0);
    });

    it('should return nb of players', function () {
        const teamA = [
            'florian',
            'alice'
        ];
        const teamB = [
            'rami',
            'sherry'
        ];
        const teamC = [
            'marine',
            'julien',
            'neo'
        ];
        const game = Game.newGame([teamA, teamB, teamC]);

        expect(game.totalNbOfPlayers()).toBe(7);
    });

    it('should return to 25 after a team miss 3 times in row', function () {
        // Given
        const game = Game.newGame([[
            'florian',
        ], [
            'alice',
        ]], [Rules.MISS_3_IN_ROW_DROP_SCORE_TO_25]);

        game.setThrust([10]); // Florian
        game.setThrust([1]); // Alice
        game.setThrust([10]); // Florian
        game.setThrust([1]); // Alice
        game.setThrust([10]); // Florian
        game.setThrust([1]); // Alice

        // When
        game.setThrust([0]); // Florian
        game.setThrust([1]); // Alice
        game.setThrust([0]); // Florian
        game.setThrust([1]); // Alice
        game.setThrust([0]); // Florian
        game.setThrust([1]); // Alice

        expect(game.getScores()).toEqual([25, 6]);
    });

    it('should return to let score as is after a team miss 3 times in row if score is below 25', function () {
        // Given
        const game = Game.newGame([[
            'florian',
        ], [
            'alice',
        ]], [Rules.MISS_3_IN_ROW_DROP_SCORE_TO_25]);

        game.setThrust([10]); // Florian
        game.setThrust([1]); // Alice
        game.setThrust([10]); // Florian
        game.setThrust([1]); // Alice

        // When
        game.setThrust([0]); // Florian
        game.setThrust([1]); // Alice
        game.setThrust([0]); // Florian
        game.setThrust([1]); // Alice
        game.setThrust([0]); // Florian
        game.setThrust([1]); // Alice

        expect(game.getScores()).toEqual([20, 5]);
    });
});