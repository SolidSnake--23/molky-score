import {Game} from "../molky/game";

import './style.css';
import {IStore, Store} from "../molky/store";
import {Team} from "../molky/team";
import {Shuffler} from "../molky/shuffler";


class UI {
    private unselectedAllPins: (() => void)[] = [];
    private missButton: HTMLButtonElement|null = null;

    private selectedPins: number[] = [];
    private game: Game = this.store.saveExists() ? Game.fromSave(this.store.load()) : Game.newGame([]);


    constructor(
        private readonly store: IStore,
        private readonly teams: Team,
    ) {
        this.render();
    }

    private render() {
        document.body.innerHTML = "";
        if (this.game.totalNbOfPlayers() !== 0) {
            this.createNextPlayerBoard();
            this.createPins();
            this.missButton = this.createCommands();
            this.createScoreBoard();
        }
        this.createNewGameCommand();
    }

    private renderNewGamePage() {
        document.body.innerHTML = "";
        const players = this.createPlayerInput();
        this.createNewGamePageCommands(players);
    }

    private createPins(): void {
        const pinBoard = document.createElement('div');
        pinBoard.classList.add('pin-board');
        document.body.append(pinBoard);
        this.unselectedAllPins = new Array(12).fill(null).map((_, value) => {
            const pin = document.createElement("div");
            const pinValue = document.createElement("div");
            pin.classList.add('pin');
            pin.append(pinValue);
            pinValue.classList.add('pin-value');
            pinValue.innerText = `${value + 1}`;
            pin.addEventListener('click', () => {
                pinValue.classList.toggle('selected');
                this.togglePin(value + 1);
                this.missButton?.classList.remove('selected')
            });
            pinBoard.append(pin);
            return () => pinValue.classList.remove('selected')
        });
    }

    private createCommands() {
        const commandBoard = document.createElement('div');
        commandBoard.classList.add('commands-board');
        document.body.append(commandBoard);
        const missButton = document.createElement('button');
        missButton.innerText = "Miss 💀";
        missButton.classList.add('btn');
        missButton.addEventListener('click', () => {
            this.unselectedAllPins.forEach(unselect => unselect());
            missButton.classList.add('selected');
            this.chooseMiss();
        });
        const submitButton = document.createElement('button');
        submitButton.innerText = "Submit ✔";
        submitButton.addEventListener('click', () => {
            this.submitScore();
        });
        const undoButton = document.createElement('button');
        undoButton.innerText = "Undo ↩";
        undoButton.addEventListener('click', () => {
            this.undo();
        });
        commandBoard?.append(missButton);
        commandBoard?.append(submitButton);
        commandBoard?.append(undoButton);
        return missButton;

    }

    private togglePin(pinValue: number) {
        if (this.selectedPins.indexOf(pinValue) >= 0) {
            this.selectedPins = this.selectedPins.filter(value => value !== pinValue);
        } else {
            this.selectedPins.push(pinValue);
        }
        this.selectedPins = this.selectedPins.filter(value => value !== 0);
    }

    private chooseMiss() {
        this.selectedPins = [0];
    }

    private createNewGameCommand() {
        const menuBoard = document.createElement('div');
        menuBoard.classList.add('menu-board');
        document.body.append(menuBoard);
        const newGame = document.createElement('button');
        newGame.innerText = "New Game";
        newGame.addEventListener('click', () => {
            this.renderNewGamePage();
        });
        menuBoard.append(newGame)
    }

    private createScoreBoard() {
        const scoreBoard = document.createElement('div');
        scoreBoard.classList.add('score-board');
        scoreBoard.append(
            ...this.game.getScores().map((scoreValue, teamIndex) => {
                const score = document.createElement('fieldset');
                const legend = document.createElement('legend');
                const header = document.createElement('h1');
                header.innerText = `${scoreValue}`;
                legend.innerText = this.game.getTeamNames()[teamIndex];
                score.append(legend);
                score.append(header);
                return score;
            })
        );
        document.body.append(scoreBoard);
    }

    private submitScore() {
        if (this.selectedPins.length === 0) {
            return;
        }
        this.game.setThrust(this.selectedPins);
        this.selectedPins = [];
        this.unselectedAllPins.forEach(unselect => unselect());
        this.render();
        this.store.persist(this.game.generateSave());
    }

    private createNextPlayerBoard() {
        const nextPlayerBoard = document.createElement('div');
        const nextPlayer = document.createElement('h1');
        nextPlayerBoard.classList.add('next-player-board');
        nextPlayer.innerText = this.game.getNextPlayer();
        document.body.append(nextPlayerBoard);
        nextPlayerBoard.append(nextPlayer);
    }

    private undo() {
        this.game.undo();
        this.render();
    }

    private createPlayerInput() {
        const fieldset = document.createElement('fieldset');
        const legend = document.createElement('legend');
        const textarea = document.createElement('textarea');

        legend.innerText = 'Players';
        fieldset.append(legend);
        fieldset.append(textarea);
        textarea.placeholder = 'Player 1\nPlayer 2\nPlayer 3';

        document.body.append(fieldset);

        return () => textarea.value.split('\n').filter(player => player);
    }

    private createNewGamePageCommands(players: () => string[]) {
        const commandBoard = document.createElement('div');
        commandBoard.classList.add('commands-board');
        document.body.append(commandBoard);
        const cancel = document.createElement('button');
        cancel.innerText = "Cancel";
        cancel.classList.add('btn');
        cancel.addEventListener('click', () => {
            this.render();
        });
        const start = document.createElement('button');
        start.innerText = "Start";
        start.addEventListener('click', () => {
            this.game = Game.newGame(this.teams.groupPlayersInTeams(players()));
            this.render();
        });
        commandBoard?.append(cancel);
        commandBoard?.append(start);
    }
}

new UI(new Store(), new Team(new Shuffler(Math.random)));